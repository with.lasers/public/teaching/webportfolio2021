# Building a Web Portfolio - Summer Session 2021

## Introduction

Getting yourself setup on the web - lots of choices to make - especially between purpose built solutions (that invariably you'll pay for) down to buildoing every single part of it

Getting an online presence is a bit more than just getting a website these days, there are a lots of things to think about

* Social media
* Other Communication channels
* Services for shopping, printing, etc that you might want to make use of

Also, as computaional artists, we are likely to have more complex needs - we might want to create embedded content using libraries such as P5, Three, or A-Frame - we might even want to create sompletely standalone sites using a specific technology, as it doesn't make sense to have them embedded elsewhere.

Today's will start the scratch the surface of this - we'll do a little bit of theory first, then we'll use an open source system to generate a small part of your web empire, and show how you can host it in the real world for free, and get more functionality than other things out there (yeah, looking at you Linktree)

## Design

Last week Atau talked about Ideation, and in the notes he had some references to IDEO - who are also the founders of Design Thinking, a process which tries to come up better solutions to problems.

Before we dive into technology and start making decisions - it's a good idea to think about what we want our online precesnce to be - what do we want to communicate via it, and what opportunities are we hoping to gain from this work. If we can't answer these questions, then we're likely to struggle to build something compelling and interesting.

I'm slowly building my web portfolio, and I thought I'd share with you the design decisions I've made so far. Although is a growing list of things, here are the key things I want to be able to do…

* Share who I am as an artist with the world
* Start to advertise the services I might choose to make available as I graduate, such as comp arts and web consultancy
* Show off some of my art - both interactive and more traditional
* Sell my work, potentially in different forms
* Write about aspects of my work, that I think people might find interesting
* Allow people to discover more about me
* Be a central point where you can see everything I have done
* Deep dive into my bigger projects

As you can see it's quite a list, and yours may not be (if so lucky you!) but it could be even bigger. You'll want to think about how you put your web portfolio together in a way that best represents this.

It became apparent to me, that the one thing that wasn't going to work well for me, was one large website - I want to be able to have an umberlla one, which is the main point of entry - but to then have seperate sites for each key piece of work.

### Domains and Sub Domains - A small diversion

As you may know, a domain (www.with-lasers.studio) is typically how we access different resources on the web, you may also know, that actually what happens is that under the covers what actually happens is we get data from a specific IP address (www.with-lasers.studio currently resolves to 3.67.153.12). This is managed by something called a DNS - Domain Name Server. They have all sorts of complexities, but easiest thing is to think of them as an address book, you look up the domain name, and get an IP address back.

A domain is made up of a few parts - most importantly to us, there is the main domain (with-lasers.studio) and the subdomain (in the example above www). This can become key to building a complex web empire with a single domain address, because we can use different subdomains for different things.

Here's how my web portfolio is organised

`www.with-lasers.studio`    global site, with info about everything in

`links.with-lasers.studio`  A link tree style page that can be accessed from places like IG, and have specific content on i want

`harmonograph.with-lasers.studio`
`plagioclase.with-lasers.studio`   Specific project sites that can focus on specifics in more depth

`store.with-lasers.studio`   Stuff to buy

`harmonograph-vr.with-lasers.studio`  A specific stand alone VR app made in A-Frame

Of course this is quite complex, but the really key things is each of these are unique sites, and seperate projects in terms of code. Which means if I maintain my main site, I don't have to worry I'm going to break other unrelated things.

## Technology

OK, so lets talk technology - internet tech is like an out of control darwinian race, where new frameworks, approaches and concepts emerge every 5s, which can make makign a decision hard… (WebDev Bingo time!)

One approach to this, is use something available as a service online - and let them do all the heavy lifting (this is SaaS btw), and there are lots of people doing this, Wordpress, Cargo, Wix, Squarespace etc. Depedning on your needs these can be perfect, but they have some downsides

* Cost - can be expensive
* Lock in - not easy to every move your content to somewhere else
* What happens when/if the service folds
* Can be easy to look like everyone elses content (especially the more mass market ones)
* Although the online, drag / drop experience can be good - can also be frustrating as well (CompArts blog anyone)

So we could write something ourselves - and depending on what we need, this could genuinely be as simple as starting out with a few files of HTML and CSS, and finding somethere to host them - and we're done! Of course this only works for simple requirements, and has the potential to become all encomessing if not careful.

### How we make websites, another diversion

So, early days, a website would be handcrafted, out of yout finest HTML, and then maybe some CSS, and eventually some JS - this was great - but anyone who had dynamic content, or got bored easily, would eventually realise that creating multiple pages consitently etc, wasn't making best use of computring power.

Some of this could be solved with tools like Macromedia's Dreamwweaver - it helped make site creation easier, but created crufty horrible code

So we started to create server side websites, where we could let the server build the page we wanted pickign up things like files of invidual data for a specific page and developing rules to handle - scripting languages like PHP appear

Eventually people realise that having a server build the page every time, isn't expecially efficent.

So we reach the point today, where we find a balance, using server side technology for very dynamic content, and tools to prebuild our pages - especially where they are static - which is really good for SSO (which is then good for us!)

## Static Site Generators

When we get hands on, we are going to build a linktree alternate using a site generator called Gatsby js - if you do a search on Site Generators you'll find a stack of them ( and the invariable Best of in 2021 pages!). We use a stack of technology called Sapper / Svelte for the Exhibition website in build at the moment (and in 2020 / DAC 2021). I use Gatsby personally, as it\s got a few advantages

First off, the disadvantage though - it's super powerful, so it's a bit like tackling OpenFrameworks for the first time, your not quite sure why it seem so odd in places, and it takes a little whiler to start feeling comftable in it.

But like OpenFrameworks (and actually not like open frameworks at all!) its really well documented, and it has loads of plugins developed for it, as well as starter projects - which means you can start creating things really quickly that do what you want to do..

Lets pick an example, by default it uses an image plugin, which does a lot of very cool things under the hood, we don't have to worry about

* It creates fast loading placeholders - these mater to things like how your site might be indexed (fast loading sites get better results in things like Google search results)
* It handles dynaimc sizing of images across different devices - so you're alsways loading an oprimum sized image for the browser

So we can build better websites without having to worry about specifics, unless we really need to.

Shopiufy integration

## Some other tech we need to worry about

Gatsby uses React to handle breaking down your website into resuable components - Armando is going / has covered this in his Thursday sessions, and I'm not going to dive intro too much here today - but it's a key bit of how it all works.

The example we are going to look at uses a framework called Tailwind for CSS - again I won't go into too much detail - I really like it, as it makes it easy for me to create different formats on different devices with out having to think about how it works behind the scenes.

Hosting - there are two elements to this

1. Code repository - I used Gitlab, but any online git repo works - of course you could just sit on your machine, but I really like having an offsite store of my work, and use the features like issues to help manage how I build things
2. Somewhere to serve the pages from - I use something called Netlify, which takes my code from gitlab and builds it, and then hosts the results of it - github / gitlab pages are also great soluitions if you don't need anything like a database etc

Domain Names - I purchased mine from Porkbun.com (goldcomparts.show is also managed by them), but I use Netlify for my DNS server - as it the allows me to manage how all my differtent sites work.

## Hands on Bit