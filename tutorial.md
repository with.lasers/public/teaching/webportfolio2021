# Creating a Link Tree Alternate

Linktree is a good example of a service, which you can make use of for free, but anything beyond the basics requires a $6 a month subscription for certain features. There are also some good reasons why making your primary link to the world sit on someone else's domain isn't the best idea.

We're going to create your own Linktree like equivalent that could sit on your own domain, and allow you to add what ever features you find useful using Gatsby js

The starter we are going to use is based on what I've written for my own links, which you can find at [personal][1] and [artist][2]

# Step 1 - Prerequisites

You'll need to have the following installed, so if you haven't done so already, go ahead and do that

* Node.js - either [directly][3], or using something like [nvm][4]
* Git - typically on a Mac this exists already, for Windows use these [instructions][6] and [these][7] on Linux
* Some sort of text editor - I typically use [Visual Studio Code][5] but you could use Sublime, Atom, Brackets amongst others

We'll also be making use of the command line from Terminal, or similar application

# Step 2 - Getting started with Gatsby

In order to use gatsby, we need to install the Gatsby CLI (Command Line Interface), which is as straighforward as running

```
npm install -g gatsby-cli
```

Note the `-g` flag which installs it globally, meaning you can run the command `gatsby` from anywhere on your computer. Let's do that now

```
gatsby -v
```

which should return something like

```
Gatsby CLI version: 3.9.0

```

(the `-v` tells Gatsby to tell us what version it is)

Once this is installed, we can now run gatsby commands, think of this like Project Generator in OpenFrameworks, with a few more tricks up it's sleeve.

# Step 3 - Using a starter template

One of the best things about gatsby is it's abaility to make use of starters, these are templates which provide a place you can start building from - they range from basic 2 page site starters, to ones which will do things like Shopify integration.

I've created one for the Linktree alternative, so lets go ahead and use that

First navigate to your flder of choice - for me, It'll be in the folder all my dev stuff is in

```
cd ~/dev/demo
```

If you don't want to type that all in, you can type `cd` and then drop the folder you want into terminal and it will complete the path for you (note if I idd this I'd get)

```
cd /Users/lxinspc/dev/demo
```

as `~` is a unix / linkux convention to mean a short cut to my home directory which is `/Users/lxinspc`

to run the starter use

```
gatsby new links https://gitlab.com/with.lasers/public/web/linksgatsbystarter.git
```

lets break that down a bit, `new` means create a new project, `links` will the name of the project, and the folder name that will be created and `https://gitlab.com/with.lasers/public/web/linksgatsbystarter.git` is the gitlab address of my starter.

This will go ahead and download the starter into the folder specified by new, and then install the npm dependencies indicated by the project.

# Step 4 - Lets run the starter

Gatsby doesn't just provide the framework for building your site, it also makes the development process easier, providing a clever development server running on your own machine which makes working on your site easier - let's do that now

```shellsession
cd links
gatsby develop
```

which moves us into the new directory we created, and starts the development server, you should see something like the following

```shellsession
success open and validate gatsby-configs - 0.070s
success load plugins - 0.885s
success onPreInit - 0.025s
success initialize cache - 0.007s
success copy gatsby files - 0.060s
success onPreBootstrap - 0.013s
success createSchemaCustomization - 0.005s
success Checking for changed pages - 0.002s
success source and transform nodes - 0.096s
success building schema - 0.388s
info Total nodes: 63, SitePage nodes: 4 (use --verbose for breakdown)
success createPages - 0.046s
success Checking for changed pages - 0.001s
success createPagesStatefully - 0.044s
success update schema - 0.016s
success write out redirect data - 0.001s
success onPostBootstrap - 0.001s
info bootstrap finished - 3.615s
success onPreExtractQueries - 0.001s
success extract queries from components - 1.126s
success write out requires - 0.004s
success run static queries - 0.019s - 1/1 52.32/s
success run page queries - 1.177s - 3/3 2.55/s
⠀
You can now view linksGatsbyStarter in the browser.
⠀
  http://localhost:8000/
⠀
View GraphiQL, an in-browser IDE, to explore your site's data and schema
⠀
  http://localhost:8000/___graphql
⠀
Note that the development build is not optimized.
To create a production build, use gatsby build
⠀

success Building development bundle - 16.323s
```

spot how it's telling us we have a server now running and we can access the pages created on `http://localhost:8000` lets have a look at that now

# Step 5 - lets have a look at the code and starting making this our own

If your using Visual Studio code, there is a really handy way to open it with the current directory frm the terminal

```shellsession
code -n .
```

the `-n` means new window, and the `.` means the current directory

(note on a MacOS, you'll need to run a command in VSCode first for this to work, see the install instructions [here][8])

Also note, you can't do this, as gatsby is running the dev server - we'll want to kepp that running - again on a Mac you can use `Cmd-T` to open a new tab, which will be in the same directory and you can do it from here.

Lets have a look at the project structure - there are a bunch of files at the project directory level we'll look at some of these later, but first, everything of interest is going to be in the `src` directory, lets have a look in here

Gatsby uses the `src` directory to contain everything that will be used to build your project - in this it's just like OpenFrameworks, and it's `src` directory. When gatsby runs, it will look to find the `src/pages` directory and create a page for each `.js` file it finds in there.

You'll see we have a `404.js` and `index.js` files - lets have a quick look at `index.js`

There's nothing obviously meaning full here - in the case of this starter, we create links pages using some data, and some scripts - so we don't have to worry about how the page is structured and can focus on the content.

Let's have a look at the `src/data` folder, in here there is a folder called `links` and in here there is a `starter.json` file - lets have a look at that

```json
{
  "id": "starter",
  "active": true  ,
  "isDefault": false,
  "name": "links gatsby starter",
  "subname": "a linktree alternate",
  "image": "../../images/placeholder.png",
  "imageStyle": "square",
  "description": "this is the starter template for linksGatsbyStarter by with-lasers",
  "groups": [
    {
      "id": "social",
      "priority": -10,
      "icon": "people-arrow",
      "title": "follow me…"
    },
    {
      "id": "development",
      "priority": 40,
      "icon": "terminal",
      "title": "code"
    },
    {
      "id": "poweredBy",
      "priority": 80,
      "title": "powered by"
    }
  ],
  "links": [
    {
      "id": "instagram",
      "group": "social",
      "link": "https://www.instagram.com/w.ith.lasers/",
      "priority": -1,
      "active": true,
      "title": "follow on instagram",
      "prefix": "fab",
      "icon": "instagram"
    },
    {
      "id": "gitlab",
      "group": "development",
      "link": "https://gitlab.com/lxinspc",
      "priority": 100,
      "title": "lxinspc on gitlab",
      "prefix": "fab",
      "icon": "gitlab"
    },
    {
      "id": "github",
      "group": "development",
      "link": "https://github.com/lxinspc",
      "priority": 100,
      "title": "lxinspc on github",
      "prefix": "fab",
      "icon": "github"
    },
    {
      "id": "gatsby",
      "group": "poweredBy",
      "link": "https://www.gatsbyjs.com",
      "priority": 1,
      "title": "gatsby.js The static dynamic site generator for dynamic web developers",
      "icon": "code"
    },
    {
      "id": "tailwind",
      "group": "poweredBy",
      "link": "https://tailwindcss.com",
      "priority": 1,
      "title": "tailwindcss A utility-first CSS framework packed with classes like flex, pt-4, text-center and rotate-90 that can be composed to build any design, directly in your markup.",
      "prefix": "fab",
      "icon": "css3-alt"
    }
  ]
}
```

Here we have all our config for managing our links pages - lets make some changes - we'll add a new link in the Powered By section for Node - add the following to the links array in the file

```json
{
      "id": "node",
      "group": "poweredBy",
      "link": "https://nodejs.org/en/",
      "priority": 3,
      "title": "Node.js® is a JavaScript runtime built on Chrome's V8 JavaScript engine.",
      "prefix": "fab",
      "icon": "node-js"
    }
 ```

Once we save, `gatsby develop` will detect the changes to the file, and rebuild the site for us - so now we can go back to `http://localhost:8000` and have a look, and we should see the new link.

# Step 6 - Make your own links page

The way this Gatsby code works, is that it looks for `JSON` files in the `src/data/links` directory and creates a page for it - this is how on my site, I have one for my personal instagram and one for my artists account.

Copy the `starter.json` file to something meaningfull (maybe your IG user name) and make sure you keep the .json extension. I'll use `nhadams.json` as my example. You also need to change the `id` field to the name you want to access it at

Again, gatsby develop will pick that up, and create the file for it - nothing obvious will change in the browser, but we can now access the page `http://localhost:8000/nhadams` as this page has been created from the json file created.

## The links file structure

There are three sections to this json file - at root level there are some key fields

```json
  "id": "nhadams",
  "active": true  ,
  "isDefault": false,
  "name": "links gatsby starter",
  "subname": "a linktree alternate",
  "image": "../../images/placeholder.png",
  "imageStyle": "square",
  "description": "this is the starter template for linksGatsbyStarter by with-lasers",
```

* Active means the page is will be built - this allows you to turn off pages if required, but keep the configuration for them.
* isDefault - this site will recreate the link page at the index (so it's the default page if nothing else is provided) if this is true (if more than one is, then no guarentees on which one it will pick!)
* name - is the name shown at the top
* subname - the secondary text under the main title
* image - a link to an image that can be shown
* imageStyle - how the image is framed
* description - the text below the image (or title / subname)

Go ahead and change these - make sure you set isDefault to true, and set it to false on `starter.json` this way, you'll always get the default page you are expect

We've seen how to add links above - lets have a more detailed look at this

```json
  "id": "node",
  "group": "poweredBy",
  "link": "https://nodejs.org/en/",
  "priority": 3,
  "title": "Node.js® is a JavaScript runtime built on Chrome's V8 JavaScript engine.",
  "prefix": "fab",
  "icon": "node-js"
```

* id a unique id, just required so we can keep track of each item
* group - all the links are grouped - we can see these in the groups array, we'll add some new groups in a moment
* link - the link we want to make available to the user
* priority - basically a sort order
* title - the text we want to show next to the link
* prefix / icon - we can show an icon from font awesome - the prefix is the style and icon is the name of it

Now lets add some groups

```json
{
  "id": "myNewGroup",
  "priority": 20,
  "image": "../../images/groups/plagioclase.png",
  "title": "My new group"
}
```

If you add this, and save, you won't seen anything new - that's because a group without any links won't display, you'll need to add or a change a link to make it appear.

Note we have the property image in here - this creates a banner image for the group

## Step 7 - Styling

In the folder `src/style` you'll find the global styles in `global.css` where you'll find a small number of styles, which you can use to change the main formatting elements of the page.

This template uses a CSS framework called Tailwind, which does a couple of things, mainly privding styles which are very closely related to actual CSS properties - which I find much easier to use and implment. It's not for everyone, but I recommend looking at the [Getting Started Info][9]

Gatsby has a number of ways stytling can be handled, and because it uses react to break things down, we can do a lot of styling within components (especially layout stuff) which means we can create a small number of useful global styles - namely background colour, and text colours, all the layout is handled in the components.

We use the css syntax `@apply` to build up new styles out of existing ones, so we can do the following

```css
body {
  @apply text-indigo-100 bg-indigo-900;  
}

.headerName {
  @apply font-sans font-bold text-5xl text-indigo-300;
}
```

`text-indigo-100` is a tailwind class for text color, using a tailwind colour (indigo) at an 10% shade, similarly `bg-indigo-900` sets the background colour.

`font-sans` uses the defined sans font in `tailwind.config.js`, `font-bold` makes bold, `text-5xl` sets the size

Have a look at tailwind (really good docs, and a great search using Cmd-K) and set your own style - you can of course add usual css into these classes as well.

## Step 8 - GraphQL - Optional

Gatsby uses GraphQL to build up the data behind your site - and in dev mode we can browse this data at `http://localhost:8000/__graphql`

This can be a somewhat over the top view at first - but it's worth starting to explore it a little, as we can write queries to get at this data - if you have a look at `src/templates/links.jsx` you'll see the query against the collection linksJson to get the data for the page.

## Step 9 - gatsby-config.js / gatsby-node.js - Optional

The way the links data gets into GraphQL and means we can process it is in the `gatsby-node.js` file, this is a node script run by the gatsby process to do your own processing.

In this case it's working with `gatsby-config.js` to find the links data, we add an entry here in plugins for `gatsby-transformer-json` which uses `gatsby-source-filesystem` to get any files in `src/data` which will create the node `allLinksJson`

`gatsby-node.js` then runs a createPage script on each file it finds in allLinksJson using the template `src/templates/links.jsx`

## Step 10 - test build

`gatsby develop` is an ongoing process to run whsilt you make changes, it's not how gatsby builds the site (for example in develop, it's inlcuded live reload functionality so you don't have to refresh so often).

if we stop develop (`ctrl-C`) we can then run

```shellsession
gatsby build
```

this creates a build folder, which has our finsihed site in - we can double check this is ok, by running

```shellsession
gatsby serve
```

Notice the build server is usually on port 9000 not 8000

which creates another server for us, and allows us to check for any errors - occasionally something might sneak through which breaks the build which works in development.

The build folder is what we want to deploy

## Step 11 - github / gitlab

Before we deploy to Netlify, we need to get onto a git repo remotely, using eother git hub, or gitlab.

Go and create a project in your preferred platform, and get the repo address (usually via the clone button)

Gatsby created a git repo for us when we ran the starter, so we just need to add this remote address we can do this from the command line with

```shellsession
git remote add origin <remoteaddr>
```

once we've done that, we can then add our files, commit and push

```shellsession
git add .
git commit -m 'initial commit'
git push --set-upstream origin main
```

and if we check gitlab / github we should have all out project files ready for us.

## Step 12 - deploytment to netlify

Log into Netlify, and we can click on the button `New Site From Git` and basically follow the instructions.

This will make netlify create a new site for us, grab our code from github / gitlab, spin up a server to run build (just like we did above), and if succesful, take the build output and m,ake available as a website.

Now everytime we push to master - Netlify will spin that server up again to rebuild your site and make the necessary updates.















[1]:https://links.with-lasers.studio/lxinspc
[2]:https://links.with-lasers.studio/with-lasers


[6]:https://www.atlassian.com/git/tutorials/install-git#windows
[7]:https://www.atlassian.com/git/tutorials/install-git#linux
[8]:https://code.visualstudio.com/docs/setup/mac

[9]:https://tailwindcss.com/docs